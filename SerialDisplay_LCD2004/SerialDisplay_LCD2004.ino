/*
 * Displays text sent over the serial port (e.g. from the Serial Monitor) on
 * an attached LCD.
 * 
 *  !!!!!! YOu have to change the HardwareSerialk size (from 64 to 128 !)
 *  C:\Users\chris\Documents\ArduinoData\packages\arduino\hardware\avr\1.8.1\cores\arduino
 * 
 */
#include <Wire.h> 
#include <LiquidCrystal_I2C.h>
const int numChars = 100;
char receivedChars[numChars]; // an array to store the received data
boolean newData = false;

LiquidCrystal_I2C lcd(0x27,20,4);  // set the LCD address to 0x27 for a 16 chars and 2 line display

void setup()
{
  lcd.init();                      // initialize the lcd 
  lcd.backlight();
  Serial.begin(9600);
}

void loop()
{
  // when characters arrive over the serial port...
  if (Serial.available()) {
    // wait a bit for the entire message to arrive
    delay(100);
    // clear the screen
    // read all the available characters
    
    recvWithEndMarker();
    //Serial.println(receivedChars);
    EncodeMessage(receivedChars);
    newData = false;
  }
}

void recvWithEndMarker() {
  //receivedChars = "";
  receivedChars[0] = '\0';
  
  static byte ndx = 0;
  char endMarker = '$';
  char rc;


  while (Serial.available() > 0 && newData == false) {
    rc = Serial.read();
    //int i = rc
    //Serial.print(rc, HEX);

    if (rc != endMarker) {

      if ((rc != 0xA) && (rc != 0xD)) {
        receivedChars[ndx] = rc;
        ndx++;
        if (ndx >= numChars) {
          ndx = numChars - 1;
        }
      } else {
        //Serial.println("  = nix gut zeichen");
      }

    }else {
      //Serial.println("END !");
      receivedChars[ndx] = rc;
      int Nr = ndx + 1;
      receivedChars[Nr] = '\0'; // terminate the string
      ndx = 0;
      newData = true;
    }
    //Serial.print("\r\n");
  }

}

void EncodeMessage(char *str){
  char *ptr;
  int count = 0;
  
  ptr = strtok(str, ";");
  //lcd.clear();

  while( ptr != NULL){
    //Serial.print("Row_");
    //Serial.print(count);
    //Serial.print("-->");
    //Serial.println(ptr);

    lcd.setCursor(0, count);
    lcd.print("                    ");

    if (strstr(ptr, "$") !=  NULL) {
      char *ptr2 = strtok(ptr, "$");
      lcd.setCursor(0, count);
      lcd.print(ptr);
      break;
    }else{
      lcd.setCursor(0, count);
      lcd.print(ptr);
    }
    
    ptr = strtok(NULL, ";");
    count ++;
  }
  /*
  Serial.print("Row_1 -> ");
  Serial.println(ptr);
  lcd.setCursor(0,0);
  lcd.print(ptr);
  
  ptr = strtok(NULL, ";");
  ptr = strtok(str, ";");
  Serial.print("Row_2 -> ");
  Serial.println(ptr);
  lcd.setCursor(0,1);
  lcd.print(ptr);
  
  ptr = strtok(str, ";");
  Serial.print("Row_3 -> ");
  Serial.println(ptr);
  lcd.setCursor(0,2);
  lcd.print(ptr);
  
  ptr = strtok(str, ";");
  Serial.print("Row_4 -> ");
  Serial.println(ptr);
  lcd.setCursor(0,3);
  lcd.print(ptr);
  */
}
