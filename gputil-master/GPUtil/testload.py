import GPUtil as GPU
import sys
import serial
import time
import psutil

print(GPU.getload())
print(round(GPU.getload()))
print(GPU.getmemoryusage())
print(round(GPU.getmemoryusage()))
print(GPU.getmemoryusagenvidiacalc())
print(GPU.getgputemperature())
print(GPU.getcardname())

print(psutil.cpu_percent())
# gives an object with many fields
print(psutil.virtual_memory().percent)


#
# connect to arduino display LCO2004
#

s = serial.Serial('COM8', 9600, timeout=1)
time.sleep(3)

#
# build string
#  row1 ; row2 ; row3 ; row4 $ 
#
charbreak = ";"
charend = "$"

while(True):
    load = round(GPU.getload())
    memusage = round(GPU.getmemoryusage())
    temperature = round(GPU.getgputemperature())
    name = GPU.getcardname()

    cpu_perc = round(psutil.cpu_percent())
    cpu_mem = round(psutil.virtual_memory().percent)

    #string = name + charbreak + "Load = " + str(load) + "%" + charbreak + "memload  = " + str(memusage) + "%" + charbreak + "temper. = " + str(temperature) + " Grad " + charend
    string = name + charbreak + "L " + str(load) + " M " + str(memusage) + " T " + str(temperature) + charbreak + "CPU i7-8600 " + charbreak + "L " + str(cpu_perc) + " M " + str(cpu_mem) + charend
    print(string)
    print("send -> " + str(s.write(str.encode(string))) )
    time.sleep(1)

s.close()
